import nose
from acp_times import * 


def test_acp_open_times():
    """Opening The juice!"""
    assert open_time(101, 1000, "2000-03-25 08:00") == "2000-03-25 10:58" 
    assert open_time(201, 1000, "2000-03-25 08:00") == "2000-03-25 13:55" 
    assert open_time(301, 1000, "2000-03-25 08:00") == "2000-03-25 17:02" 
    assert open_time(501, 1000, "2000-03-25 08:00") == "2000-03-25 23:30" 
    assert open_time(650, 1000, "2000-03-25 08:00") == "2000-03-26 04:35" 




def test_acp_closing_times():
    """Closing The Juice!"""
    assert 2 == 2
    assert close_time(101, 1000, "2000-03-25 08:00") == "2000-03-25 14:44"
    assert close_time(201, 1000, "2000-03-25 08:00") == "2000-03-25 21:24"
    assert close_time(301, 1000, "2000-03-25 08:00") == "2000-03-26 04:04"
    assert close_time(501, 1000, "2000-03-25 08:00") == "2000-03-26 17:24"
    assert close_time(650, 1000, "2000-03-25 08:00") == "2000-03-27 04:23"
